package com.hmrc.checkout.store;

import com.hmrc.checkout.product.Product;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class ProductStoreImplTest {

	private ProductStore underTest;

	private Product apple = new Product("Apple", "0.6");
	private Product orange = new Product("Orange", "0.25");

	@Before
	public void setUp() {
		underTest = new ProductStoreImpl();
	}

	@Test
	public void addProduct_ShouldAddProduct() {
		// given

		// when
		underTest.addProduct(apple);

		// then
		assertNotNull(underTest.getProduct(apple));
	}

	@Test
	public void addProduct_ShouldNotAddProductIfAlreadyPresent() {
		// given

		// when
		underTest.addProduct(apple);

		// then
		assertNull(underTest.addProduct(apple));
	}

	@Test
	public void addProducts_shuldAddProducts() {
		// given

		// when
		underTest.addProducts(apple, orange);

		// then
		assertNotNull(underTest.getProduct(apple));
		assertNotNull(underTest.getProduct(orange));
	}

	@Test
	public void getProduct_ShuldGetProductByName() {
		// given
		String appleName = "apple";

		// when
		underTest.addProduct(apple);
		Product foundProduct = underTest.getProduct(appleName);

		// then
		assertEquals(foundProduct, apple);
	}

	@Test
	public void addProduct_ProductShouldExistWhenAdded() {
		// given

		// when
		underTest.addProduct(apple);

		// then
		assertNotNull(underTest.getProduct(apple));
	}


	@Test
	public void productExists_ShuldReturnTrueWhenAdded() {
		// given
		underTest.addProduct(apple);

		// when
		boolean exists = underTest.productExists(apple);

		// then
		assertTrue(exists);
	}


	@Test
	public void productExists_ShuldReturnTrueWhenAddedByNameIgnoringCase() {
		// given
		underTest.addProduct(apple);
		String breadName = "APPLE";

		// when
		boolean exists = underTest.productExists(breadName);

		// then
		assertTrue(exists);
	}

	@Test
	public void productExists_ShuldReturnFalseWhenNotAdded() {
		// given
		underTest.addProduct(apple);

		// when
		boolean exists = underTest.productExists(orange);

		// then
		assertFalse(exists);
	}

}
