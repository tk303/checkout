package com.hmrc.checkout.extras;

import static org.junit.Assert.*;
import org.junit.Test;

public class LevenshteinDistanceTest {

    private LevenshteinDistance underTest = new LevenshteinDistance();

    @Test
    public void shouldCalculateDistanceAsLengthWhenOneIsEmpty() {
        // given
        String s = "";
        String t = "abcd";

        // when
        int distance = underTest.calculateDistance(s, t);

        // then
        assertEquals(4, distance);
    }

    @Test
    public void shouldCalculateDistanceAsOne() {
        // given
        String s = "abcc";
        String t = "abcd";

        // when
        int distance = underTest.calculateDistance(s, t);

        // then
        assertEquals(1, distance);
    }

    @Test
    public void shouldCalculateDistanceAsThree() {
        // given
        String s = "TomaszKubiak";
        String t = "ThomasKubik";

        // when
        int distance = underTest.calculateDistance(s, t);

        // then
        assertEquals(3, distance);
    }
}