package com.hmrc.checkout.pricecalculator;

import static org.junit.Assert.*;
import java.math.BigDecimal;
import com.hmrc.checkout.cart.SimpleShoppingCartImpl;
import com.hmrc.checkout.offer.BuyProductThenGetDiscount;
import com.hmrc.checkout.product.Product;
import org.junit.Before;
import org.junit.Test;

public class PriceCalculatorTest {

	private PriceCalculator underTest;

	private SimpleShoppingCartImpl cart;

	private Product apple1 = new Product("Apple", "0.6");
	private Product apple2 = new Product("apple", "2.3");
	private Product apple3 = new Product("APPLE", "2.3");
	private Product orange = new Product("orange", "0.25");
	private Product orange1 = new Product("orange", "2.3");


	@Before
	public void setUp() {
		cart = new SimpleShoppingCartImpl();
		underTest = new PriceCalculator(cart);
	}

	@Test
	public void shouldCalculateTotal() {
		// given
		cart.addProduct(apple1);
		cart.addProduct(apple2);
		cart.addProduct(apple3);
		cart.addProduct(orange1);

		// when
		BigDecimal total = underTest.getTotalWithDiscounts();

		// then
		assertEquals(new BigDecimal("4.1"), total);
	}

	@Test
	public void testAssignment() {
		// given
		Product apples = new Product("Apples", "0.6");
		Product oranges = new Product("Oranges", "0.25");

		cart.addProduct(apples);
		cart.addProduct(oranges);

		cart.addProduct(apples);
		cart.addProduct(oranges);

		// when
		BigDecimal total = underTest.getTotalWithDiscounts();

		// then
		assertEquals(new BigDecimal("1.70"), total);

	}

	@Test
	public void shouldCalculateTotalWithDiscountOnApple() {
		// given
		cart.addProduct(apple1);
		cart.addProduct(apple2);
		cart.addProduct(apple3);
		cart.addProduct(orange1);

		//create some offers:
		BuyProductThenGetDiscount offerOnApple = new BuyProductThenGetDiscount(apple1, 1, 0, 100);

		// when
		underTest.addOffer(offerOnApple);
		BigDecimal totalWithDiscounts = underTest.getTotalWithDiscounts();

		// then
		assertEquals(new BigDecimal("3.5"), totalWithDiscounts);
	}

	@Test
	public void shouldCalculateTotalWithDiscountOnOranges() {
		// given
		cart.addProduct(orange);
		cart.addProduct(orange);
		cart.addProduct(orange);
		cart.addProduct(orange1);

		//create some offers:
		BuyProductThenGetDiscount offerOnApple = new BuyProductThenGetDiscount(orange, 2, 0, 100);

		// when
		underTest.addOffer(offerOnApple);
		BigDecimal totalWithDiscounts = underTest.getTotalWithDiscounts();

		// then
		assertEquals(new BigDecimal("0.75"), totalWithDiscounts);
	}

}
