package com.hmrc.checkout.cart;

import static org.junit.Assert.*;

import com.hmrc.checkout.product.Product;
import org.junit.Before;
import org.junit.Test;

public class SimpleShoppingCartImplTest {

	private SimpleShoppingCartImpl underTest;

	@Before
	public void setUp() {
		underTest = new SimpleShoppingCartImpl();
	}

	@Test
	public void shouldReturnCorrectNumberOfDistinctItems() {
		// given
		Product apple1 = new Product("Apple", "1.9");
		Product apple2 = new Product("apple", "2.3");
		Product apple3 = new Product("APPLE", "2.3");
		Product orange1 = new Product("orange", "2.3");

		// when
		underTest.addProduct(apple1);
		underTest.addProduct(apple2);
		underTest.addProduct(apple3);
		underTest.addProduct(orange1);

		// then
		assertEquals(underTest.getNumberOfDistinctProducts(), 2);
	}

	@Test
	public void shouldReturnCorrectNumberOfItems() {
		// given
		Product apple = new Product("apple", "1.9");
		Product bread = new Product("bread", "2.3");
		Product orange = new Product("orange", "2.3");

		// when
		underTest.addProduct(apple);
		underTest.addProduct(bread);
		underTest.addProduct(orange);
		underTest.addProduct(apple);

		// then
		assertEquals(underTest.getTotalNumberOfProducts(), 4);
	}

}
