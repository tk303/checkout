package com.hmrc.checkout.product;

import static org.junit.Assert.*;
import java.math.BigDecimal;
import java.util.HashSet;
import org.junit.Test;

public class ProductTest {

	private Product apple = new Product("apple", "2.3");
	private Product apple2 = new Product("APPLE", 12);

	@Test
	public void product_ConstructedWithSpecifiedValues() {
		// given
		String name = "PRODUCT";
		String id = name.toLowerCase();
		String price = "2.30";
		BigDecimal priceBigDecimal = new BigDecimal(price);

		// when
		Product product = new Product(name, price);

		// then
		assertEquals(id, product.getId());
		assertEquals(name, product.getName());
		assertEquals(priceBigDecimal, product.getPrice());
	}

	@Test
	public void product_ConstructorsAreEqual() {
		// given

		// when

		// then
		assertTrue(apple.equals(apple2));
	}

	@Test
	public void equals_ProductsShouldBeEqual() {
		// given
		Product apple2 = new Product("APPLE", "2.3");

		//then
		assertEquals(apple, apple2);
	}

	@Test
	public void equals_ProductsNotEqualsObject() {
		// given
		Product apple2 = new Product("APPLE", "2.3");

		//then
		assertNotEquals(apple, new Object());
	}


	@Test
	public void hashCode_HashMapRetrieval() {
		// given
		HashSet<Product> products = new HashSet<>();

		// when
		products.add(apple);

		// then
		assertTrue(products.contains(apple2));
	}


}
