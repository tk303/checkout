package com.hmrc.checkout.offer;

import com.hmrc.checkout.cart.ShoppingCart;
import com.hmrc.checkout.product.Product;
import org.junit.Test;
import java.math.BigDecimal;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BuyProductThenGetDiscountTest {

    private BuyProductThenGetDiscount underTest;
    private ShoppingCart cart = mock(ShoppingCart.class);

    private Product requiredProduct = mock(Product.class);
    private int noOfProductsToApply = 1;
    private int maxApplication = 0;
    private int discountPercentage = 10;
    private Offer offer = mock(Offer.class);

    private static BigDecimal oneHundred = new BigDecimal("100");
    private static BigDecimal fifty = new BigDecimal("50");
    private static BigDecimal twenty = new BigDecimal("20");

    @Test
    public void exclusiveWithOffer_shouldReturnFalse() {
        // given
        underTest = new BuyProductThenGetDiscount(requiredProduct, noOfProductsToApply,
                maxApplication, discountPercentage);

        // when
        boolean offerExclusive = underTest.exclusiveWithOffer(offer);

        // then
        assertFalse(offerExclusive);
    }

    @Test
    public void calculateDiscount_ShouldReturnZeroWhenRequiredProductNotPurchased() {
        // given
        when(cart.getNumberOfProducts(requiredProduct)).thenReturn(0);

        underTest = new BuyProductThenGetDiscount(requiredProduct, noOfProductsToApply,
                maxApplication, discountPercentage);

        // when
        BigDecimal discount = underTest.calculateDiscount(cart);

        // then
        assertEquals(BigDecimal.ZERO, discount);
    }


    @Test
    public void calculateDiscount_ShouldReturnDiscountWhenProductsInTheBasket() {
        // given
        when(cart.getNumberOfProducts(requiredProduct)).thenReturn(2);
        when(requiredProduct.getPrice()).thenReturn(oneHundred);

        underTest = new BuyProductThenGetDiscount(requiredProduct, noOfProductsToApply,
                maxApplication, discountPercentage);

        // when
        BigDecimal discount = underTest.calculateDiscount(cart);

        // then
        assertEquals(BigDecimal.TEN, discount);
    }

    @Test
    public void calculateDiscount_ShouldReturnDiscountWhenMultipleProductsInTheBasket() {
        // given
        when(cart.getNumberOfProducts(requiredProduct)).thenReturn(10);
        when(requiredProduct.getPrice()).thenReturn(oneHundred);

        underTest = new BuyProductThenGetDiscount(requiredProduct, noOfProductsToApply,
                maxApplication, discountPercentage);

        // when
        BigDecimal discount = underTest.calculateDiscount(cart);

        // then
        assertEquals(fifty, discount);
    }

    @Test
    public void calculateDiscount_ShouldReturnZeroWhenNotEnoughRequiredProducts() {
        // given
        noOfProductsToApply = 2;
        when(cart.getNumberOfProducts(requiredProduct)).thenReturn(1);
        when(requiredProduct.getPrice()).thenReturn(oneHundred);

        underTest = new BuyProductThenGetDiscount(requiredProduct, noOfProductsToApply,
                maxApplication, discountPercentage);

        // when
        BigDecimal discount = underTest.calculateDiscount(cart);

        // then
        assertEquals(BigDecimal.ZERO, discount);
    }

    @Test
    public void calculateDiscount_ShouldRespectNoOfProductsToApply() {
        // given
        noOfProductsToApply = 2;
        when(cart.getNumberOfProducts(requiredProduct)).thenReturn(3);
        when(requiredProduct.getPrice()).thenReturn(oneHundred);

        underTest = new BuyProductThenGetDiscount(requiredProduct, noOfProductsToApply,
                maxApplication, discountPercentage);

        // when
        BigDecimal discount = underTest.calculateDiscount(cart);

        // then
        assertEquals(BigDecimal.TEN, discount);
    }

    @Test
    public void calculateDiscount_ShouldRespectMaxApplications() {
        // given
        maxApplication = 2;
        when(cart.getNumberOfProducts(requiredProduct)).thenReturn(10);
        when(requiredProduct.getPrice()).thenReturn(oneHundred);

        underTest = new BuyProductThenGetDiscount(requiredProduct, noOfProductsToApply,
                maxApplication, discountPercentage);

        // when
        BigDecimal discount = underTest.calculateDiscount(cart);

        // then
        assertEquals(twenty, discount);
    }
}
