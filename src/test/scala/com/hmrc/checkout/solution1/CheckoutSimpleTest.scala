package com.hmrc.checkout.solution1

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CheckoutSimpleTest extends FunSuite {

  test("Should return price of apple"){
    val products = Array("apple")
    val result = CheckoutSimple.totalNoDiscounts(products)
    val expected = BigDecimal("0.60")
    assert(result == expected)
  }

  test("Should return price of oranges"){
    val products = Array("orange")
    val result = CheckoutSimple.totalNoDiscounts(products)
    val expected = BigDecimal("0.25")
    assert(result == expected)
  }

  test("Should return price of 5 apples and 2 oranges"){
    val products = Array("apple", "orange", "apple", "orange", "apple", "apple", "apple")
    val result = CheckoutSimple.totalNoDiscounts(products)
    val expected = BigDecimal("3.50")
    assert(result == expected)
  }

  test("Should return price of 5 apples and 2 oranges ignoring case"){
    val products = Array("Apple", "orange", "apple", "Orange", "apple", "APPLE", "apple")
    val result = CheckoutSimple.totalNoDiscounts(products)
    val expected = BigDecimal("3.50")
    assert(result == expected)
  }

  test("Should return correct price when prices specified"){
    val prices = Map("apple" -> BigDecimal("6"), "orange" -> BigDecimal("1.25"), "bread" -> BigDecimal("1.3"))
    val products = Array("Apple", "orange", "apple", "Orange", "apple", "APPLE", "apple", "bread")
    val result = CheckoutSimple.totalNoDiscounts(products, prices)
    val expected = BigDecimal("33.80")
    assert(result == expected)
  }

}
