package com.hmrc.checkout.solution2

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CheckoutWithDiscountTest extends FunSuite {

  test("Should return price of apple"){
    val products = Array("apple")
    val result = CheckoutWithDiscount.totalWithDiscounts(products)
    val expected = BigDecimal("0.60")
    assert(result == expected)
  }

  test("Should return price of oranges"){
    val products = Array("orange")
    val result = CheckoutWithDiscount.totalWithDiscounts(products)
    val expected = BigDecimal("0.25")
    assert(result == expected)
  }

  test("Should return price of 5 apples and 3 oranges"){
    val products = Array("apple", "orange", "apple", "orange", "apple", "orange", "apple", "apple")
    val result = CheckoutWithDiscount.totalWithDiscounts(products)
    val expected = BigDecimal("2.30")
    assert(result == expected)
  }

  test("Should return price of 5 apples and 3 oranges ignoring case"){
    val products = Array("Apple", "orange", "apple", "Orange", "apple", "ORANGE", "APPLE", "apple")
    val result = CheckoutWithDiscount.totalWithDiscounts(products)
    val expected = BigDecimal("2.30")
    assert(result == expected)
  }

  test("Should return correct price when prices specified"){
    val prices = Map("apple" -> BigDecimal("6"), "orange" -> BigDecimal("1.25"), "bread" -> BigDecimal("1.3"))
    val products = Array("Apple", "orange", "apple", "Orange", "apple", "ORANGE", "APPLE", "apple", "bread")
    val result = CheckoutWithDiscount.totalWithDiscounts(products, prices)
    val expected = BigDecimal("21.80")
    assert(result == expected)
  }

  test("Should return price of 5 apples and 5 oranges"){
    val products = Array("apple", "orange", "apple", "orange", "apple", "orange", "apple", "orange", "apple", "orange")
    val result = CheckoutWithDiscount.totalWithDiscounts(products)
    val expected = BigDecimal("2.80")
    assert(result == expected)
  }

}
