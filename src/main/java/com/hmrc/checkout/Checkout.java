package com.hmrc.checkout;

import java.math.BigDecimal;
import java.util.Map;

import com.hmrc.checkout.cart.ShoppingCart;
import com.hmrc.checkout.cart.SimpleShoppingCartImpl;
import com.hmrc.checkout.extras.CurrencyFormatter;
import com.hmrc.checkout.extras.ProductMatch;
import com.hmrc.checkout.offer.BuyProductThenGetDiscount;
import com.hmrc.checkout.offer.Offer;
import com.hmrc.checkout.pricecalculator.PriceCalculator;
import com.hmrc.checkout.product.Product;
import com.hmrc.checkout.store.ProductStore;
import com.hmrc.checkout.store.ProductStoreImpl;

/**
 * Main Class
 */
public class Checkout {

	private static CurrencyFormatter sterlingCurrencyFormatter = CurrencyFormatter.builder()
			.unitSymbol("£")
			.subUnitSymbol("p")
			.numberOfDecimalPlaces(2)
			.currencyUnitFirst(true)
			.build();

	public static void main(String[] args) {
		if (args.length < 1) {
			System.out.println("Please specify at least one item for Checkout");
			return;
		}

		// Create products
		Product apple = new Product("Apple", "0.6");
		Product orange = new Product("Orange", "0.25");

		ProductStore availableProducts = new ProductStoreImpl();
		availableProducts.addProducts(apple, orange);

		ShoppingCart cart = createShoppingCart(args, availableProducts);

		PriceCalculator priceCalculator = new PriceCalculator(cart);

		// add some offers
		priceCalculator.addOffer(getBuyOneGetOneFreeOffer(apple, "Buy one, get one free on Apples"));
		priceCalculator.addOffer(getThreeForTwoOffer(orange, "3 for the price of 2 on Oranges"));

		// Calculate final price
		BigDecimal totalNoDiscounts = priceCalculator.getTotalNoDiscounts();
		BigDecimal totalWithDiscounts = priceCalculator.getTotalWithDiscounts();
		Map<Offer, BigDecimal> applicableOffers = priceCalculator.getApplicableOffers();

		System.out.println("Subtotal: " + sterlingCurrencyFormatter.formatCurrency(totalNoDiscounts));
		if (!applicableOffers.isEmpty()) {
			for (Offer offer : applicableOffers.keySet()) {
				System.out.println(offer.getDescription() + ": -"
						+ sterlingCurrencyFormatter.formatCurrency(applicableOffers.get(offer)));
			}
		} else {
			System.out.println("(no offers available)");
		}

		System.out.println("Total: " + sterlingCurrencyFormatter.formatCurrency(totalWithDiscounts));
	}

	/**
	 * Creates {@link ShoppingCart} from passed arguments and available products
	 */
	private static ShoppingCart createShoppingCart(String[] args, ProductStore availableProducts) {
		ShoppingCart cart = new SimpleShoppingCartImpl();

		// iterate all arguments and add available products to shopping cart
		for (int i = 0; i < args.length; i++) {
			String productName = args[i];
			if (!availableProducts.productExists(productName)) {

				ProductMatch bestMatch = availableProducts.findBestMatch(productName);

				if (bestMatch != null && bestMatch.getDistance() < 5) {
					System.out.println("Product: \"" + productName + "\" is not available in our offer." +
							"\n Did you mean:\"" + bestMatch.getProduct().getName() + "\"?");
				} else {
					System.out.println("Product: \"" + productName + "\" is not available in our offer.");
				}
			} else {
				cart.addProduct(availableProducts.getProduct(productName));
			}
		}
		return cart;
	}

	private static BuyProductThenGetDiscount getBuyOneGetOneFreeOffer(Product discountProduct, String description) {
		BuyProductThenGetDiscount appleOffer = new BuyProductThenGetDiscount(discountProduct, 1, 0, 100);
		appleOffer.setDescription(description);
		return appleOffer;
	}

	private static BuyProductThenGetDiscount getThreeForTwoOffer(Product discountProduct, String description) {
		BuyProductThenGetDiscount appleOffer = new BuyProductThenGetDiscount(discountProduct, 2, 0, 100);
		appleOffer.setDescription(description);
		return appleOffer;
	}


}
