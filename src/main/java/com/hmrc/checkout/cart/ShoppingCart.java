package com.hmrc.checkout.cart;

import java.util.Map;
import java.util.Set;

import com.hmrc.checkout.product.Product;

/**
 * Shopping cart interface
 */
public interface ShoppingCart {

	/**
	 * Adds product to ShoppingCart
	 * @param product
	 */
	void addProduct(Product product);

	/**
	 * Returns a number of specified product in the basket
	 * @param product
	 * @return
	 */
	int getNumberOfProducts(Product product);

	/**
	 * Returns how many distinct products is in the cart
	 * @return
	 */
	int getNumberOfDistinctProducts();

	/**
	 * Returns total number of products
	 * @return
	 */
	int getTotalNumberOfProducts();

	/**
	 * Returns an {@link Set} of {@link Map.Entry} of all {@link Product}s and their quantities
	 * @return
	 */
	Set<Map.Entry<Product, Integer>> getEntrySet();

}
