package com.hmrc.checkout.cart;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.hmrc.checkout.product.Product;

/**
 * Simple implementation of the {@link ShoppingCart} interface
 * Uses a {@link HashMap} for storing cart items
 */
public class SimpleShoppingCartImpl implements ShoppingCart {

	// list of products and their quantity
	private HashMap<Product, Integer> items = new HashMap<>();

	public void addProduct(Product product) {
		if (items.containsKey(product)) {
			Integer count = items.get(product);
			items.put(product, count + 1);
		} else {
			items.put(product, 1);
		}
	}

	public int getNumberOfDistinctProducts() {
		return items.size();
	}

	public int getTotalNumberOfProducts() {
		return items.values().stream().collect(Collectors.summingInt(Integer::intValue));
	}

	@Override
	public int getNumberOfProducts(Product product) {
		return items.containsKey(product) ? items.get(product) : 0;
	}

	public Set<Map.Entry<Product, Integer>> getEntrySet() {
		return items.entrySet();
	}

}
