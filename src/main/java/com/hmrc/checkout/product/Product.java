package com.hmrc.checkout.product;

import lombok.Getter;
import java.math.BigDecimal;

/**
 * Implementation of a shopping Product
 */
@Getter
public class Product {

	private String id;
	private String name;
	private BigDecimal price;

	public Product(String name, String price) {
		this.name = name;
		this.id = name.toLowerCase();
		this.price = new BigDecimal(price);
	}

	public Product(String name, int price) {
		this.name = name;
		this.id = name.toLowerCase();
		this.price = new BigDecimal(price);
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof Product)) {
			return false;
		}
		Product product = (Product) o;
		return this.id.equals(product.id);
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

}
