package com.hmrc.checkout.pricecalculator;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.hmrc.checkout.cart.ShoppingCart;
import com.hmrc.checkout.offer.Offer;
import lombok.RequiredArgsConstructor;

/**
 * Calculates prices based on {@link ShoppingCart} and provided {@link Offer}s
 */
@RequiredArgsConstructor
public class PriceCalculator {

	private final Set<Offer> offers = new HashSet<>();
	private final ShoppingCart cart;

	private Map<Offer, BigDecimal> applicableOffers;
	private BigDecimal totalWithDiscounts;
	private BigDecimal totalNoDiscounts;

	/**
	 * Adds {@link Offer} to selected offers
	 * @param offer
	 */
	public void addOffer(Offer offer) {
		offers.add(offer);
	}

	/**
	 * Based on selected {@link Offer}s, returns ones that are applicable, based on items in the cart,
	 * with the discount amount.
	 */
	public Map<Offer, BigDecimal> getApplicableOffers() {
		if (totalWithDiscounts == null) {
			calculateTotalPrice();
		}
		return applicableOffers;
	}

	/**
	 * Calculates and returns total cost of {@link ShoppingCart} after applicable offers were used
	 * @return
	 */
	public BigDecimal getTotalWithDiscounts() {
		if (totalWithDiscounts == null) {
			calculateTotalPrice();
		}
		return totalWithDiscounts;
	}

	/**
	 * Calculates and returns total cost of {@link ShoppingCart} before applying any discounts
	 */
	public BigDecimal getTotalNoDiscounts() {
		if (totalWithDiscounts == null) {
			calculateTotalPrice();
		}
		return totalNoDiscounts;
	}

	/**
	 * Performs calculations based on selected items and offers
	 */
	private void calculateTotalPrice() {
		//calculate price without applying any promotions
		totalNoDiscounts = cart.getEntrySet().stream()
				.map(product -> product.getKey().getPrice().multiply(new BigDecimal(product.getValue())))
				.reduce(BigDecimal.ZERO, BigDecimal::add);

		applicableOffers = new HashMap<>();
		BigDecimal appliedDiscount = BigDecimal.ZERO;
		// calculate price with discounts
		for (Offer offer : offers) {
			// calculate discount entitled with this offer
			BigDecimal currentDiscount = offer.calculateDiscount(cart);
			if (currentDiscount != null && currentDiscount != BigDecimal.ZERO) {
				appliedDiscount = appliedDiscount.add(currentDiscount);
				applicableOffers.put(offer, currentDiscount);
			}
		}
		totalWithDiscounts = totalNoDiscounts.subtract(appliedDiscount);
	}

}

