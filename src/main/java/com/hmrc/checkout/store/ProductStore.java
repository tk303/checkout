package com.hmrc.checkout.store;

import com.hmrc.checkout.extras.ProductMatch;
import com.hmrc.checkout.product.Product;

/**
 * Interface representing a {@link Product} store
 * Database or other data structure for saving and accessing products.
 */
public interface ProductStore {

	Product addProduct(Product product);

	void addProducts(Product... products);

	Product getProduct(String productName);

	Product getProduct(Product product);

	boolean productExists(Product product);

	boolean productExists(String productName);

	ProductMatch findBestMatch(String productName);
}
