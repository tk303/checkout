package com.hmrc.checkout.store;

import com.hmrc.checkout.extras.LevenshteinDistance;
import com.hmrc.checkout.extras.ProductMatch;
import com.hmrc.checkout.product.Product;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Simple implementation of a {@link ProductStore}
 */
public class ProductStoreImpl implements ProductStore {

	private Map<String, Product> products = new HashMap<>();

	public Product addProduct(Product product) {
		if (!products.containsKey(product.getId())) {
			return products.put(product.getId(), product);
		} else {
			return null;
		}
	}

	public boolean productExists(Product product) {
		return productExists(product.getId());
	}

	public boolean productExists(String productName) {
		return products.containsKey(productName.toLowerCase());
	}

	@Override
	public void addProducts(Product... products) {
		for (int i = 0; i < products.length; i++) {
			Product product = products[i];
			this.products.put(product.getId(), product);
		}
	}

	public Product getProduct(String productName) {
		return products.get(productName.toLowerCase());
	}

	public Product getProduct(Product product) {
		return products.get(product.getId());
	}

	public ProductMatch findBestMatch(String productName) {
		List<ProductMatch> productMatches = products.entrySet().stream()
				.map(x -> new ProductMatch(LevenshteinDistance.calculateDistance(x.getValue().getId(), productName), x.getValue()))
				.sorted(Comparator.comparing(ProductMatch::getDistance))
				.collect(Collectors.toList());

		if (productMatches != null && !productMatches.isEmpty()) {
			return productMatches.get(0);
		}
		return null;
	}
}
