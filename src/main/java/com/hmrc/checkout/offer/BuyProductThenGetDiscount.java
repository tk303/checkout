package com.hmrc.checkout.offer;

import java.math.BigDecimal;
import com.hmrc.checkout.cart.ShoppingCart;
import com.hmrc.checkout.product.Product;
import lombok.RequiredArgsConstructor;

/**
 * Offered that is discounts a price of a product when additional items are bought.
 */
@RequiredArgsConstructor
public class BuyProductThenGetDiscount extends Offer {

    private static final BigDecimal oneHundred = new BigDecimal(100);

    // Product that needs to be bought to get the offer
    private final Product offerProduct;

    // number of required products to qualify for the offer
    private final int noOfProductsToApply;

    // maximum number of discounts that can be applied (maximum number of products that can be bought at discounted price)
    private final int maxApplication;

    // discount in percents
    private final int discountPercentage;

    @Override
    public BigDecimal calculateDiscount(ShoppingCart cart) {
        int numberOfProducts = cart.getNumberOfProducts(this.offerProduct);


        int numberOfAplicableDiscounts = numberOfProducts / (noOfProductsToApply + 1);

        // when max items greater then zero, there is a limit on how many times discount can be applied
        if (maxApplication > 0) {
            numberOfAplicableDiscounts = Math.min(numberOfAplicableDiscounts, maxApplication);
        }

        if (numberOfAplicableDiscounts > 0) {
            return calculateDiscountAmount(offerProduct.getPrice(), numberOfAplicableDiscounts);
        } else {
            return BigDecimal.ZERO;
        }
    }

    private BigDecimal calculateDiscountAmount(BigDecimal price, int productQuantity) {
        BigDecimal appliedDiscount;
        appliedDiscount = price.multiply(new BigDecimal(productQuantity));
        appliedDiscount = appliedDiscount.multiply(new BigDecimal(this.discountPercentage));
        appliedDiscount = appliedDiscount.divide(oneHundred);
        return appliedDiscount;
    }

}
