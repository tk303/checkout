package com.hmrc.checkout.offer;

import java.math.BigDecimal;

import com.hmrc.checkout.cart.ShoppingCart;
import lombok.Getter;
import lombok.Setter;

/**
 * Shopping offer interface
 */
@Getter
@Setter
public abstract class Offer {

    private String description;

    /**
     * Calculates the discount applied to a shopping cart
     * @param cart
     * @return
     */
    public abstract BigDecimal calculateDiscount(ShoppingCart cart);

    /**
     * Checks whether offer is exclusive with another
     * @param offer
     * @return
     */
    public boolean exclusiveWithOffer(Offer offer) {
        return false;
    }


}

