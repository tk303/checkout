package com.hmrc.checkout.extras;

import lombok.Builder;
import java.math.BigDecimal;

/**
 * Class used for formatting monetary amounts in different currencies.
 * {@link Builder} is provided for configuring all formatting option and creating appropriate formatter.
 */
@Builder
public class CurrencyFormatter {

    private String unitSymbol;
    private String subUnitSymbol;

    private int numberOfDecimalPlaces;
    private boolean currencyUnitFirst;

    public String formatCurrency(BigDecimal d) {
        if (d.compareTo(BigDecimal.ONE) > -1) {
            return (currencyUnitFirst ? unitSymbol : "")
                    + (d.setScale(numberOfDecimalPlaces, BigDecimal.ROUND_CEILING)).toString()
                    + (currencyUnitFirst ? "" : unitSymbol);
        } else {
            return ((d.setScale(numberOfDecimalPlaces, BigDecimal.ROUND_CEILING)).toString() + subUnitSymbol)
                    .substring(2);
        }
    }
}
