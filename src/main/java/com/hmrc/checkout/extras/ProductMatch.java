package com.hmrc.checkout.extras;

import com.hmrc.checkout.product.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Helper class for storing distance (difference) between two {@link Product} names
 */
@Getter
@AllArgsConstructor
public class ProductMatch {

    private int distance;
    private Product product;
}
