package com.hmrc.checkout.extras;

/**
 * Util class for calculating distance between two strings.
 * Distance is a number of letters that need to be changed to get from string1 to string2.
 */
public class LevenshteinDistance {

    /**
     * Calculates distance between two strings.
     * Distance is a number of letters that need to be changed to get from string1 to string2.
     * Could be used as a measure to see how much two strings are different from each other.
     *
     * @param string1
     * @param string2
     * @return
     */
    public static int calculateDistance(String string1, String string2) {
        return levenshteinDistance(string1.toCharArray(), string1.length(), string2.toCharArray(), string2.length());
    }

    /**
     * Recursive method for calculating Levenshtein Distance
     * https://en.wikipedia.org/wiki/Levenshtein_distance
     *
     * @param string1 first string
     * @param len_s1  length of first string
     * @param string2 second string
     * @param len_s2  length of second string
     * @return
     */
    private static int levenshteinDistance(char[] string1, int len_s1, char[] string2, int len_s2) {
        int cost;

        /* base case: empty strings */
        if (len_s1 == 0) return len_s2;
        if (len_s2 == 0) return len_s1;

        /* test if last characters of the strings match */
        if (string1[len_s1 - 1] == string2[len_s2 - 1]) {
            cost = 0;
        } else {
            cost = 1;
        }

        return Math.min(levenshteinDistance(string1, len_s1 - 1, string2, len_s2) + 1,
                Math.min(levenshteinDistance(string1, len_s1, string2, len_s2 - 1) + 1,
                        levenshteinDistance(string1, len_s1 - 1, string2, len_s2 - 1) + cost));
    }

}

