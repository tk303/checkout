package com.hmrc.checkout

import com.hmrc.checkout.solution1.CheckoutSimple

object CheckoutApp {

  def main(args: Array[String]): Unit = {
    println("Welcome to our store")

    val total = CheckoutSimple.totalNoDiscounts(args)

    println(s"Your total spending is: $total")

  }

}
