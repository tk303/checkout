package com.hmrc.checkout

import com.hmrc.checkout.solution2.CheckoutWithDiscount

object CheckoutApp2 {

  def main(args: Array[String]): Unit = {
    println("Welcome to our store, we now offer incredible deals")

    val total = CheckoutWithDiscount.totalWithDiscounts(args)

    println(s"Your total is: $total")

  }

}
