package com.hmrc.checkout.solution1

object CheckoutSimple {

  private val defaultPriceStore = Map("apple" -> BigDecimal("0.60"), "orange" -> BigDecimal("0.25"))
  private val zero = BigDecimal("0")

  def totalNoDiscounts(products: Array[String], priceStore: Map[String, BigDecimal] = defaultPriceStore): BigDecimal = {
    products.map(s => s.trim).map(s => s.toLowerCase) // trim and set to lower case
      .flatMap(priceStore.get)                        // replace product with it's price
      .foldLeft(zero) ( _ + _)                        // add prices together
  }


}
