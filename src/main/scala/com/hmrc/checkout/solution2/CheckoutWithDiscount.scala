package com.hmrc.checkout.solution2

object CheckoutWithDiscount {

  private val defaultPriceStore = Map("apple" -> BigDecimal("0.60"), "orange" -> BigDecimal("0.25"))
  private val zero = BigDecimal("0")

  def totalWithDiscounts(products: Array[String], priceStore: Map[String, BigDecimal] = defaultPriceStore): BigDecimal = {

    // create some discounts
    val apppleDiscount = buyNGetOneFreeDiscount("apple", 1)(_)
    val orangeDiscount = buyNGetOneFreeDiscount("orange", 2)(_)

    // calculate final price
    products.map(s => s.trim).map(s => s.toLowerCase)
      .groupBy(s => s)                                          // group by product type
      .mapValues(s => s.foldLeft(0)((total, _) => total + 1))   // count number of each grouped items
      .map(apppleDiscount)                                      // apply discount (by decreasing total by free items)
      .map(orangeDiscount)                                      // apply similar discount for oranges
      .map( entry => (priceStore(entry._1), entry._2))          // retrieve price for each item
      .map(entry => entry._2 * entry._1 )                       // calculate item's price
      .foldLeft(zero)(_ + _)                                    // add up all grouped items' totals
  }


  def buyNGetOneFreeDiscount(productName: String, n: Int)(cartProduct: (String, Int)) = {
    if (cartProduct._1.equals(productName))
      (cartProduct._1, (cartProduct._2 - (cartProduct._2 / (n + 1))))
    else
      cartProduct
  }

}
